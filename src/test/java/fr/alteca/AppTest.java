package fr.alteca;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Test ADOP ")
public class AppTest
{

    private String msgTest ;
    private App app;
    
    @BeforeEach    
    public void init() {
    	msgTest="Projet Adop !";
        System.out.println("@ Chaine de test => : "+msgTest);  
        app=new App();
    }
   
        
    @Test
    @DisplayName("Test 1: Conformité de retour ")
    public void testAppMain()
    {     
        try {        	
           assertEquals(msgTest , app.adop());
        } catch (AssertionError e) {
           fail(" Un FAUX message !");
         }
    }

    
   @AfterEach
    public void finTest() {
	    app=null;
    	System.out.println("@ Fin test \n************");    	
    }   
 
}
